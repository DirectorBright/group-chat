let accountType;
document.getElementById('notice').style.display = 'none';
async function makeAuthenticatedRequest() {
    const token = getCookie('token'); // Implement a function to retrieve the token
  
    try {
      const response = await fetch('/api/verify.js', {
        method: 'GET', // or 'POST' for a POST request
        headers: {
          'Authorization': `Bearer ${token}`, // Attach the token in the 'Authorization' header
          'Content-Type': 'application/json',
        },
        // Additional request options (e.g., body for POST requests)
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log('Server response:', data);
      } else {
        console.error('Server error:', response.status, response.statusText);
        if (window.location.hostname == '127.0.0.1') {
          //allow!!
        } else {
          window.location.href = '/'
        }
      }
    } catch (error) {
      console.error('Error:', error);
      if (window.location.hostname == '127.0.0.1') {
        //allow!!
      } else {
        window.location.href = '/'
      }
    }
  }
  makeAuthenticatedRequest();

function getCookie(cookieName) {
    const cookies = document.cookie.split(';');
    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
      if (name === cookieName) {
        return decodeURIComponent(value);
      }
    }
    return null;
  }

document.getElementById('username').innerHTML = getCookie('username');
document.getElementById('pfp').src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + getCookie('username') + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b'

const functionUrl = '/api/users.js';
const username = getCookie('username');

const headers = new Headers({
  'Content-Type': 'application/json',
  'username': username, // Include the username in the headers
});

fetch(functionUrl, {
  method: 'GET',
  headers: headers,
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((userData) => {
    accountType = userData.type
    if (getCookie('username') !== userData.username) {
      Cookies.set('username', userData.username);
      window.location.reload();
    }
    if (accountType !== 'Admin') {
      if (accountType !== 'Owner') {
        window.location.replace('/')
      }
    }
  })
  .catch((error) => {
    console.error('Error fetching user data:', error);
    if (window.location.hostname == '127.0.0.1') {
      //allow!!
    } else {
      window.location.href = '/'
    }
  });


document.getElementById('cp-confirm').addEventListener('click', async () => {
    let password = document.getElementById('cp-old').value;
    let newPassword = document.getElementById('cp-new').value;
    let username = getCookie('username')
    //verify current token
    makeAuthenticatedRequest();
    //login again to verify the username
    const response = await fetch('/api/login.js', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });
  
      if (response.ok) {
        const data = await response.json();
        document.cookie = 'token=' + data.token + '; path=/'
        if (data.username !== username) {
            window.location.href = '/'
        }
      } else {
        const errorData = await response.text();
        alert('Old password is incorrect!')
      }
      const requestBody = {
        new_password: newPassword,
      };
      
      // Create the headers for the request
      const headers = new Headers({
        'Content-Type': 'application/json',
        'username': username, // Include the username in the headers
        'action': 'change-password', // Include the action header for changing the password
      });
      
      // Send a POST request to update the password
      fetch(functionUrl, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(requestBody),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((response) => {
          console.log('Password update response:', response);
          document.getElementById('cp-old').value = '';
          document.getElementById('cp-new').value = '';
          alert('Password updated successfully');
        })
        .catch((error) => {
          console.error('Error updating password:', error);
        });
      
})

document.getElementById('au-confirm').addEventListener('click', () => {
  let auUser = document.getElementById('au-user').value;
  auUser.replace(' ', '-');
  alert('Username: "'+ auUser +'"');
  let auPass = document.getElementById('au-pass').value;
  let auType = document.getElementById('au-type').value;
  let auIRL = document.getElementById('au-irl').value;
  const fileInput = document.getElementById('au-pfp');
  makeAuthenticatedRequest();
  const functionUrl = '/api/users-admin.js';
  const username = auUser;
  const password = auPass;
  const type = auType;
  const irl = auIRL;

const data = {
  username,
  password,
  type,
  irl,
};

const headers = new Headers({
  'Content-Type': 'application/json',
});

async function pfp() {
  const file = fileInput.files[0];
  const pathToSave = auUser;
  if (!file) {
    alert('Please select a file to upload.');
    return;
  }

  const formData = new FormData();
  formData.append('file', file);

  // Add a custom header to the request
  const headers = new Headers();
  headers.append('X-Path-To-Save', pathToSave);

  try {
    const response = await fetch('/api/images.js', {
      method: 'POST',
      body: formData,
      headers: headers, // Include the custom header
    });

    if (response.ok) {
      console.log('File uploaded successfully.');
    } else {
      console.error('Error uploading file:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}
pfp();

fetch(functionUrl, {
  method: 'POST',
  headers: headers,
  body: JSON.stringify(data),
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((responseData) => {
    console.log('User created:', responseData);
    alert(responseData.message)
  })
  .catch((error) => {
    console.error('Error creating user:', error);
  });
})

document.getElementById('coup-confirm').addEventListener('click', () => {
  let user = document.getElementById('coup-user').value;
  let pass = document.getElementById('coup-np').value;
  const requestBody = {
    new_password: pass,
  };
  const headers = new Headers({
    'Content-Type': 'application/json',
    'username': user, // Include the username in the headers
    'action': 'change-password'
  });
  
  // Send a POST request to update the password
  fetch(functionUrl, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(requestBody),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then((response) => {
      console.log('Password update response:', response);
      document.getElementById('coup-user').value = '';
      document.getElementById('coup-np').value = '';
      alert(response.message)
    })
    .catch((error) => {
      console.error('Error updating password:', error);
    });
})
document.getElementById('pfp-confirm').addEventListener('click', () => {

  const fileInput = document.getElementById('pfp1');
  async function pfp() {
 
    console.log(fileInput)
    const file = fileInput.files[0];
    const pathToSave = getCookie('username');
    if (!file) {
      alert('Please select a file to upload.');
      return;
    }
  
    const formData = new FormData();
    formData.append('file', file);
  
    // Add a custom header to the request
    const headers = new Headers();
    headers.append('X-Path-To-Save', pathToSave);
  
    try {
      const response = await fetch('/api/images.js', {
        method: 'POST',
        body: formData,
        headers: headers, // Include the custom header
      });
  
      if (response.ok) {
        console.log('File uploaded successfully.');
      } else {
        console.error('Error uploading file:', response.statusText);
        return
      }
    } catch (error) {
      console.error('An error occurred:', error);
      return
    }
  }
  async function delPFP() {
    const imagePathToDelete = 'pfps/' + getCookie('username') + '.png';

const headers = new Headers();
headers.append('X-Path-To-Delete', imagePathToDelete);

fetch('/api/images-del.js', {
  method: 'DELETE',
  headers: headers,
})
  .then((response) => {
    if (response.ok) {
      console.log('File deleted successfully.');
    } else {
      console.error('Error deleting file:', response.statusText);
      return
    }
  })
  .catch((error) => {
    console.error('An error occurred:', error);
    return
  });
  }
  delPFP();
  pfp();
  alert('Profile picture updated successfully!')
})

document.getElementById('ban-confirm').addEventListener('click', () => {
  document.getElementById('notice').style.display = '';
})

document.getElementById('notice-confirm').addEventListener('click', () => {
  let banUser = document.getElementById('ban-user').value
  let unban;
  if (document.getElementById('unban').checked) {
    unban = true
  } else {
    unban = false
  }
  makeAuthenticatedRequest();
  const banFunction = async () => {
    const data = { username: banUser };
  
    try {
      const response = await fetch('/api/ban.js', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
  
      const result = await response.json();
      console.log(result); // Handle the response
      alert(banUser + ' has been banned.')
    } catch (error) {
      console.error('Error:', error);
      alert('Failed to ban user.')
    }
  };

  const unbanFunction = async () => {
    const data = { username: banUser };
  
    try {
      const response = await fetch('/api/ban.js', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
  
      const result = await response.json();
      console.log(result); // Handle the response
      alert(banUser + ' has been unbanned.')
    } catch (error) {
      console.error('Error:', error);
      alert('Failed to unban user.')
    }
  };

  if (unban) {
    unbanFunction();
  } else {
    banFunction();
  }
  document.getElementById('notice').style.display = 'none';
})