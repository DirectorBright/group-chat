if (!getUserFromURL()) {
    window.location.search = '?user=' + getCookie('username')
}

document.getElementById('user-img').src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + getUserFromURL() + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b'
document.getElementById('user-name').innerHTML = getUserFromURL()
document.title = getUserFromURL() + ' | Metnals'
function getCookie(cookieName) {
    const cookies = document.cookie.split(';');
  
    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
  
      // Check if the current cookie's name matches the desired cookieName
      if (name === cookieName) {
  
        return decodeURIComponent(value);
      }
    }
  
    return null;
  }
  const functionUrl = '/api/users.js';
const username = getUserFromURL()
const headers = new Headers({
  'Content-Type': 'application/json',
  'username': username, // Include the username in the headers
});

fetch(functionUrl, {
  method: 'GET',
  headers: headers,
})
  .then((response) => {
    if (!response.ok) {
      return response.json().then((errorResponse) => {
        throw new Error(errorResponse.error || 'Network response was not ok');
      });
    }
    return response.json();
  })
  .then((userData) => {
    accountType = userData.type
    if (accountType == 'Admin') {
      document.getElementById('user-type-img').src = '/img/users/types/admin.png'
    }
    if (accountType == 'Owner') {
      document.getElementById('user-type-img').src = '/img/users/types/owner.png'
    }
    if (!userData.bio || userData.bio == '') {

    } else {
      document.getElementById('stats-bio').innerHTML = userData.bio
      document.getElementById('edit-bio').innerHTML = userData.bio
    }
    document.getElementById('stats-since').innerHTML = formatTimestamp(userData.since._seconds)
    document.getElementById('stats-name').innerHTML = 'hidden';
  })
  .catch((error) => {
    console.error('Error fetching user data:', error);
    if (window.location.hostname == '127.0.0.1') {
      //allow!!
    } else {
      console.log(error.message);
      if (error.message === 'User not found') {
        window.location.replace('/users/error/')
      }
      if (error.message === 'An error occurred.') {
        document.getElementById('stats-bio').innerHTML = 'This user has been banned'
        document.getElementById('stats-since').innerHTML = formatTimestamp(userData.since._seconds)
      }
        document.getElementById('user-type-img').src = '/img/users/types/banned.png'
        document.getElementById('stats-bio').innerHTML = 'Failed to load user information!'
    }
  });
  function getUserFromURL() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('user');
  }
  function goTo(n) {
    window.location.replace('/users/?user=' + n)
  }
  $('input').focus(function(){
    $(this).parents('.form-group').addClass('focused');
  });
  
  $('input').blur(function(){
    var inputValue = $(this).val();
    if ( inputValue == "" ) {
      $(this).removeClass('filled');
      $(this).parents('.form-group').removeClass('focused');  
    } else {
      $(this).addClass('filled');
    }
  })  

  function search() {
    let searchingFor = document.getElementById('username-search').value;
    window.location.replace('/users/?user=' + searchingFor);
  }

  function formatTimestamp(timestamp) {
    const date = new Date(timestamp * 1000);
  
    const year = date.getFullYear();
  
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
    const month = monthNames[date.getMonth()];
    const day = date.getDate();
  
    const formattedDate = `${month} ${day}, ${year}`;
  
    return formattedDate;
  }
  document.getElementById('username-search').addEventListener('keypress', (event) => {
    if (event.key == 'Enter') {
     search();
    }
  })
  function containsHTMLElements(inputString) {
    const htmlRegex = /<\/?[a-z][\s\S]*>/i;
    return htmlRegex.test(inputString);
  }
  if (getCookie('username') !== getUserFromURL()) {
    document.getElementById('stats-edit').remove()
    console.warn('Cookie:' + getCookie('username') + ', URL:' + getUserFromURL() + '.')
  }
  document.getElementById('stats-edit').addEventListener('click', () => {
    document.getElementById('edit').style.display = 'inline'
  })
  document.getElementById('edit-finish').addEventListener('click', () => {
    const functionUrl = '/api/users.js';
const username = getCookie('username');
const newBio = document.getElementById('edit-bio').value;
if (containsHTMLElements(newBio)) {
  alert('You are not allowed to have DOM elements in your bio!')
  return
} else {

const updateBioRequest = {
  bio: newBio,
};

const headers = new Headers({
  'Content-Type': 'application/json',
  'username': username,
  'action': 'update-bio',
});

fetch(functionUrl, {
  method: 'POST',
  headers: headers,
  body: JSON.stringify(updateBioRequest),
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((response) => {
    console.log('Server response:', response);
  })
  .catch((error) => {
    console.error('Error updating bio:', error);
  });
    document.getElementById('edit').style.display = 'none'
}
  })
