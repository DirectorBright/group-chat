const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccount3456.json");
const express = require('express');
const cors = require('cors');
const path = require('path');

const app = express(); // Create an Express app

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "metnals.appspot.com",
});

// Define the route to delete an image by its path
app.delete('/api/images-del.js', async (req, res) => {
  try {
    const pathToDelete = req.get('X-Path-To-Delete'); // Retrieve the custom header value

    if (!pathToDelete) {
      return res.status(400).send('X-Path-To-Delete header is missing.');
    }

    const remoteFilename = pathToDelete;

    // Get a reference to the Firebase Storage bucket
    const bucket = admin.storage().bucket();

    // Create a reference to the image in Firebase Storage
    const file = bucket.file(remoteFilename);

    // Delete the image
    await file.delete();

    res.status(200).send('File deleted successfully.');
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('An error occurred.');
  }
});

module.exports = app;
