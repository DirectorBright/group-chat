const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount3456.json'); // Provide the path to your service account key
const jwt = require('jsonwebtoken');
const { send } = require('micro');
const crypto = require('crypto-js'); // Import the crypto-js library

const SECRET_KEY = 'irejgoisejrhposierfpoisjervoiusenrtgpinOUIGOIHPORNPOITNH8949oweurng98'; 

// Initialize Firebase Admin with your service account credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://metnals.firebaseio.com', // Replace with your Firebase project URL
});

const db = admin.firestore();

async function sha256Hash(password) {
  return crypto.SHA256(password).toString(); // Using crypto-js for hashing
}

async function sha256Check(password, hashToCheck) {
  try {
    const hashHex = await sha256Hash(password);
    const isMatch = hashHex === hashToCheck;
    return isMatch;
  } catch (error) {
    console.error('Hash checking error:', error);
    return false;
  }
}

async function checkPassword(userPassword, storedHash) {
  try {
    const match = await sha256Check(userPassword, storedHash);
    return match;
  } catch (error) {
    console.error('Error checking password:', error);
    return false;
  }
}

async function validatePassword(storedHash, userPassword) {
  try {
    const isMatch = await checkPassword(userPassword, storedHash);
    return isMatch;
  } catch (error) {
    console.error('Error:', error);
    return false;
  }
}

module.exports = async (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', 'https://metnals.kette.app');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'POST') {
    try {
      const { username, password } = req.body;
      const collectionRef = db.collection('users');

      const querySnapshot = await collectionRef.where('username', '==', username).get();

      if (querySnapshot.empty) {
        send(res, 401, { error: 'Invalid credentials' });
        return;
      }

      const userDoc = querySnapshot.docs[0];
      const userData = userDoc.data();

      if (await validatePassword(userData.password, password)) {
        if (userData.ban) {
          send(res, 403, { error: 'This user has been banned' });
          return;
        }

        const token = jwt.sign({ username }, SECRET_KEY, { expiresIn: '4h' });
        send(res, 200, { username, token });
      } else {
        send(res, 401, { error: 'Invalid credentials' });
      }
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred.' });
    }
  } else {
    send(res, 405, { error: 'Method not allowed.' });
  }
};