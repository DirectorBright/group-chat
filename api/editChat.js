const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount3456.json');

// Initialize Firebase Admin with your credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

module.exports = async (req, res) => {
  if (req.method === 'PUT') {
    const { user, name } = req.body;
    const requestingUser = req.cookies.username;

    try {
      // Query for an existing chat document with the users
      const existingChatSnapshot = await db.collection('chats')
        .where('users', 'array-contains-any', [user, requestingUser])
        .get();

      // Check each document for the presence of both users in the 'users' array
      const existingChat = existingChatSnapshot.docs.find(doc => {
        const docUsers = doc.data().users;
        return docUsers.includes(user) && docUsers.includes(requestingUser);
      });

      // If an existing chat with the same users exists, don't create a new document
      if (existingChat) {
        res.status(409).json({ error: 'Chat between these users already exists' });
      } else {
        // If no existing chat document found, create a new one
        const chatRef = db.collection('chats');
        const chatData = {
          name: name,
          users: [user, requestingUser],
        };

        const docRef = await chatRef.add(chatData);
        console.log('Document written with ID:', docRef.id);
        res.status(200).json({ success: true, message: 'Message saved successfully' });
      }
    } catch (error) {
      console.error('Error adding document:', error);
      res.status(500).json({ error: 'An error occurred while saving the message' });
    }
  } else if (req.method === 'GET') {
    const userFromHeader = req.headers.chat;
    const requestingUser = req.cookies.username;

    try {
      const chatRef = db.collection('chats');

      // Fetch the chat document based on the users
      const snapshot = await chatRef
        .where('users', 'array-contains', userFromHeader)
        .get();

      if (!snapshot.empty) {
        let chatData;
        snapshot.forEach((doc) => {
          const chatUsers = doc.data().users;
          if (chatUsers.includes(requestingUser)) {
            chatData = doc.data();
          }
        });

        if (chatData) {
          res.status(200).json(chatData);
        } else {
          res.status(200).json({ message: 'No authorized chat document found' });
        }
      } else {
        res.status(200).json({ message: 'No chat document found' });
      }
    } catch (error) {
      console.error('Error fetching document:', error);
      res.status(500).json({ error: 'An error occurred while fetching the document' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed' });
  }
};
