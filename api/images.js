const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccount3456.json");
const multer = require('multer');
const express = require('express');
const cors = require('cors');
const path = require('path');

const app = express(); // Create an Express app

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "metnals.appspot.com",
});

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// Define the route to handle image uploads
app.post('/api/images.js', upload.single('file'), async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).send('No file uploaded.');
    }

    const fileBuffer = req.file.buffer;
    const pathToSave = req.get('X-Path-To-Save'); // Retrieve the custom header value

    if (!pathToSave) {
      return res.status(400).send('X-Path-To-Save header is missing.');
    }

    const remoteFilename = `pfps/${pathToSave}.png`;

    // Upload the file to Firebase Storage
    const bucket = admin.storage().bucket();
    const file = bucket.file(remoteFilename);

    await file.save(fileBuffer, {
      metadata: {
        contentType: req.file.mimetype,
      },
    });

    res.status(200).send('File uploaded successfully.');
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('An error occurred.');
  }
});

module.exports = app;
