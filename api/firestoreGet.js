const admin = require('firebase-admin');
const { send } = require('micro');
const jwt = require('jsonwebtoken');
const SECRET_KEY = 'irejgoisejrhposierfpoisjervoiusenrtgpinOUIGOIHPORNPOITNH8949oweurng98';
const serviceAccount = require('./serviceAccount3456.json');

const firebaseConfig = {
  apiKey: "AIzaSyBD10oStv9yVnT79tJaWlm4egPL43KQ2kI",
  authDomain: "metnals.firebaseapp.com",
  projectId: "metnals",
  storageBucket: "metnals.appspot.com",
  messagingSenderId: "844042684269",
  appId: "1:844042684269:web:40311a282a27fff43dfc64"
};

// Initialize Firebase Admin with your credentials
admin.initializeApp({
credential: admin.credential.cert(serviceAccount),
databaseURL: `https://${firebaseConfig.projectId}.firebaseio.com`,
});

const db = admin.firestore();

module.exports = async (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', 'https://metnals.kette.app');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  if (req.method === 'GET') {
    try {
      // Retrieve the token from the 'Authorization' header
      const token = req.headers.authorization ? req.headers.authorization.replace('Bearer ', '') : null;
      const documents = [];
      const collectionRef = db.collection('messages'); 
      const readLogsRef = db.collection('read-logs');
      const querySnapshot = await collectionRef.orderBy('timestamp', 'desc').get(); // Order by timestamp in descending order
  
      querySnapshot.forEach((doc) => {
        const data = doc.data();
        if (data.username) {
          documents.push(data);
        } else {
          console.error('Document without username:', doc.id, data);
        }
      });

      if (!token) {
        send(res, 401, { error: 'Authorization token missing' });
        return;
      }

      // Verify the JWT token
      jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
          send(res, 401, { error: 'Invalid token' });
        } else {
          readLogsRef.add({
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            username: decoded.username,
          });
          res.status(200).json({ documents }); 
        }
      });
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred.' });
    }
  } else {
    send(res, 405, { error: 'Method not allowed.' });
  }
};
