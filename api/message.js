const jwt = require('jsonwebtoken');
const SECRET_KEY = 'irejgoisejrhposierfpoisjervoiusenrtgpinOUIGOIHPORNPOITNH8949oweurng98';
const { send } = require('micro');

let messageQueue = [];
let chatCookie = null;
let recipients = new Set(); // Record users who have received the current message
let currentMessage = null; // Record the current message details

module.exports = async function handler(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'https://metnals.kette.app');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  
  let isProcessing = false; // Define isProcessing to control queue processing

  if (req.method === 'POST') {
    try {
      const { username, message } = req.body;
      const messageID = btoa(Math.random() * 309);
      const token = req.headers.authorization ? req.headers.authorization.replace('Bearer ', '') : null;

      chatCookie = req.cookies.chat;
      recipients.clear(); // Clear the list of users who have received the last message
      currentMessage = { message, username, messageID };

      jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
          send(res, 401, { error: 'Invalid token' });
        } else {
          messageQueue.push({ message, username, messageID });
          res.status(200).json({ message, username, messageID });

          if (!isProcessing) { // Begin queue processing if it's not running
            isProcessing = true;
            processMessageQueue(res);
          }
        }
      });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else if (req.method === 'GET') {
    const token = req.headers.authorization ? req.headers.authorization.replace('Bearer ', '') : null;
    const requestingUsername = req.cookies.username;

    jwt.verify(token, SECRET_KEY, (err, decoded) => {
      if (err) {
        send(res, 401, { error: 'Invalid token' });
      } else {
        if (currentMessage && !recipients.has(requestingUsername)) {
          recipients.add(requestingUsername);
          res.status(200).json(currentMessage);
        } else {
          res.status(200).json({ message: '', username: '', messageID: '' });
        }
      }
    });
  } else {
    res.status(405).json({ error: 'Method not allowed.' });
  }
};

function processMessageQueue(res) {
  setTimeout(() => {
    if (messageQueue.length > 0) {
      messageQueue.shift();
      processMessageQueue(res);
    } else {
      res.end(); // End the response when the queue is empty
      isProcessing = false; // Reset isProcessing
    }
  }, 1000);
}
