const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount3456.json');
const crypto = require('crypto-js'); // Import the crypto-js library

// Initialize Firebase Admin with your credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://metnals.firebaseio.com', // Replace with your database URL
});

const db = admin.firestore();

module.exports = async (req, res) => {
  if (req.method === 'GET') {
    const username = req.headers.username; // Retrieve the username from request headers

    if (!username) {
      res.status(400).json({ error: 'Username not provided in headers' });
      return;
    }

    try {
      const userQuery = db.collection('users').where('username', '==', username).limit(1);
      const userQuerySnapshot = await userQuery.get();
      
      if (!userQuerySnapshot.empty) {
        const userDoc = userQuerySnapshot.docs[0];
        const userData = userDoc.data();
        userData.password = 'hidden'
        if (userData.ban) {
          send(res, 403, { error: 'This user has been banned' });
          return; // Added return to prevent further execution
        }
        res.status(200).json(userData);
      } else {
        res.status(404).json({ error: 'User not found' });
      }
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else if (req.method === 'POST') {
    try {
      // Get the username from the request headers
      const username = req.headers.username;
      const action = req.headers.action;
  
      if (!username) {
        return res.status(401).json({ error: 'Unauthorized' });
      }
  
      if (action === 'change-password') {
        // Get the new password from the request body
        let { new_password } = req.body;
      
        if (!new_password) {
          return res.status(400).json({ error: 'New password is required' });
        }
      
        // Query the "users" collection to find the document with the matching "username" field
        const userQuery = db.collection('users').where('username', '==', username).limit(1);
        const userQuerySnapshot = await userQuery.get();
      
        if (userQuerySnapshot.empty) {
          return res.status(404).json({ error: 'User not found' });
        }
      
        // Update the user's password in the Firestore database
        const userDoc = userQuerySnapshot.docs[0];
        const hashedPassword = crypto.SHA256(new_password).toString();
        await userDoc.ref.update({ password: hashedPassword });
      
        return res.status(200).json({ message: 'Password updated successfully' });
      } else if (action === 'update-bio') {
        // Get the bio from the request body
        const { bio } = req.body;
  
        if (!bio) {
          return res.status(400).json({ error: 'Bio is required' });
        }
  
        // Find the user document and update the bio field
        const userQuery = db.collection('users').where('username', '==', req.cookies.username).limit(1);
        const userQuerySnapshot = await userQuery.get();
  
        if (userQuerySnapshot.empty) {
          return res.status(404).json({ error: 'User not found' });
        }
  
        const userDoc = userQuerySnapshot.docs[0];
        await userDoc.ref.update({ bio: bio });
  
        return res.status(200).json({ message: 'Bio updated successfully' });
      } else {
        return res.status(400).json({ error: 'Invalid action' });
      }
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ error: 'An error occurred.' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed.' });
  }
};