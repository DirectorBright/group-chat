const admin = require('firebase-admin');
const { send } = require('micro');

const serviceAccount = require('./serviceAccount3456.json'); // Provide the path to your service account key

// Initialize Firebase Admin with your service account credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://metnals.firebaseio.com', // Replace with your Firebase project URL
});

const db = admin.firestore();

module.exports = async (req, res) => {
  if (req.method === 'POST') {
    try {
      const { username } = req.body;
      const usersRef = db.collection('users');

      const querySnapshot = await usersRef.where('username', '==', username).get();

      if (querySnapshot.empty) {
        send(res, 404, { error: 'User not found' });
        return;
      }

      querySnapshot.forEach((doc) => {
        const userRef = usersRef.doc(doc.id);
        userRef.update({ ban: true });
      });

      send(res, 200, { message: 'User banned' });
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred' });
    }
  } else if (req.method === 'DELETE') {
    try {
      const { username } = req.body;
      const usersRef = db.collection('users');

      const querySnapshot = await usersRef.where('username', '==', username).get();

      if (querySnapshot.empty) {
        send(res, 404, { error: 'User not found' });
        return;
      }

      querySnapshot.forEach((doc) => {
        const userRef = usersRef.doc(doc.id);
        userRef.update({ ban: admin.firestore.FieldValue.delete() });
      });

      send(res, 200, { message: 'Ban removed for user' });
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred' });
    }
  } else {
    send(res, 405, { error: 'Method not allowed' });
  }
};
