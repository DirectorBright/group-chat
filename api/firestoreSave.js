const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount3456.json');
const firebaseConfig = {
    apiKey: "AIzaSyBD10oStv9yVnT79tJaWlm4egPL43KQ2kI",
    authDomain: "metnals.firebaseapp.com",
    projectId: "metnals",
    storageBucket: "metnals.appspot.com",
    messagingSenderId: "844042684269",
    appId: "1:844042684269:web:40311a282a27fff43dfc64"
};

// Initialize Firebase Admin with your credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: `https://${firebaseConfig.projectId}.firebaseio.com`,
});

// Access Firestore
const db = admin.firestore();

module.exports = async (req, res) => {
  // Parse the request body to get the username and message
  const { username, message } = req.body;

  try {
    // Add the message to the "messages" collection
    const messageData = {
      username: username,
      message: message,
      timestamp: admin.firestore.FieldValue.serverTimestamp(),
    };

    const docRef = await db.collection('messages').add(messageData);
    console.log('Document written with ID: ', docRef.id);

    res.status(200).json({ success: true, message: 'Message saved successfully' });
  } catch (error) {
    console.error('Error adding document: ', error);
    res.status(500).json({ error: 'An error occurred while saving the message' });
  }
};