const admin = require("firebase-admin");
const { send } = require('micro');
const serviceAccount = require('./serviceAccount3456.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://metnals.firebaseio.com",
});

module.exports = async (req, res) => {
  if (req.method === 'GET') {
    const chatName = req.headers['x-chat'];
    const usernameCookie = req.cookies.username;
  
    if (!usernameCookie) {
      return send(res, 401, { error: 'Unauthorized' });
    }
  
    if (!chatName) {
      send(res, 400, { error: 'X-Chat header missing.' });
      return;
    }
  
    const db = admin.firestore();
    const chatsCollection = db.collection('chats');
  
    try {
      const querySnapshot = await chatsCollection.where('name', '==', chatName).get();
    
      if (querySnapshot.empty) {
        send(res, 404, { error: 'Chat not found.' });
        return;
      }
    
      let chatData = {};
      querySnapshot.forEach((doc) => {
        chatData = doc.data();
      });
    
      const chatRef = admin.firestore().collection('chats').doc(chatName);
      const messagesRef = chatRef.collection('messages');
    
      const messageSnapshots = await messagesRef.orderBy('timestamp', 'desc').get();

      const messages = [];
      messageSnapshots.forEach((doc) => {
        messages.push(doc.data());
      });
    
      // Ensure that usernameCookie is in the 'users' array
      if (chatData.users.includes(usernameCookie)) {
        const response = {
          chatData: chatData,
          messages: messages,
        };
    
        send(res, 200, response); // Send both chatData and messages as a JSON response
      } else {
        send(res, 401, { error: 'Unauthorized' });
      }
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred.' });
    }
  } else if (req.method === 'POST') {
    const chatName = req.headers['x-chat'];
    const usernameCookie = req.cookies.username;
  
    if (!usernameCookie) {
      return send(res, 401, { error: 'Unauthorized' });
    }
  
    if (!chatName) {
      return send(res, 400, { error: 'X-Chat header missing.' });
    }
  
    const db = admin.firestore();
    const chatsCollection = db.collection('chats');
  
    try {
      const querySnapshot = await chatsCollection.where('name', '==', chatName).get();
    
      if (querySnapshot.empty) {
        return send(res, 404, { error: 'Chat not found.' });
      }
    
      let chatData = {};
      querySnapshot.forEach((doc) => {
        chatData = doc.data();
      });
  
      // Ensure that usernameCookie is in the 'users' array
      if (chatData.users.includes(usernameCookie)) {
        const chatRef = admin.firestore().collection('chats').doc(chatName);
        await createMessagesSubcollection(chatRef);
    
        const newMessage = {
          message: req.body.message,
          username: usernameCookie,
          timestamp: admin.firestore.FieldValue.serverTimestamp(),
        };
    
        // Add a new message document to the 'messages' collection within Firestore
        await chatRef.collection('messages').add(newMessage);
    
        send(res, 200, chatData);
      } else {
        return send(res, 401, { error: 'Unauthorized' });
      }
    } catch (error) {
      console.error('Error:', error);
      return send(res, 500, { error: 'An error occurred.' });
    }
  }
};
