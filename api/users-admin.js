const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount3456.json');
const crypto = require('crypto-js');

// Initialize Firebase Admin with your credentials
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://metnals.firebaseio.com',
});

const db = admin.firestore();

async function sha256Hash(password) {
  return crypto.SHA256(password).toString(); // Using crypto-js for hashing
}

module.exports = async (req, res) => {
  if (req.method === 'POST') {
    try {
      // Get the request body containing user data
      let { username, password, type, irl } = req.body;

      // Check if the required parameters are provided
      if (!username || !password || !type) {
        return res.status(400).json({ error: 'Username, password, and type are required' });
      }

      // Check if the username is already in use
      const userQuery = db.collection('users').where('username', '==', username).limit(1);
      const userQuerySnapshot = await userQuery.get();

      if (!userQuerySnapshot.empty) {
        return res.status(409).json({ error: 'Username is already in use' });
      }

      // Create a new user document in the Firestore "users" collection
      const newUserRef = db.collection('users').doc();
      const userData = {
        username,
        password: crypto.SHA256(password).toString(),
        type,
        since: admin.firestore.FieldValue.serverTimestamp(), // Add "since" field with current timestamp
        bio: 'Customise your bio at https://metnals-hospital.vercel.app/users/', // Add "bio" field as a blank string
        irl,
      };

      await newUserRef.set(userData);

      return res.status(201).json({ message: 'User created successfully' });
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ error: 'An error occurred.' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed.' });
  }
};