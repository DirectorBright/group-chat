const signInForm = document.getElementById('signInForm');
signInForm.addEventListener('submit', handleSignIn);
document.getElementById('loading').style.display = 'none';

async function handleSignIn(event) {
  event.preventDefault();

  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  try {
    const response = await fetch('/api/login.js', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    if (response.ok) {
      const data = await response.json();
      // Save the username and token in cookies
      Cookies.set('username', username);
      Cookies.set('token', data.token);
      successLogin();
    } else {
      const errorData = await response.text(); // Read the response as text
      console.error('Login failed:', errorData);
      const errorMess = JSON.parse(errorData)
      console.log(errorMess)
      if (errorMess.error == 'Invalid credentials') {
        document.getElementById('failed').innerHTML = 'Username or password was incorrect'
        document.getElementById('failed').style.animation="fade 2s linear forwards";
      }
      document.getElementById('failed').innerHTML = errorMess.error
      document.getElementById('failed').style.animation="fade 2s linear forwards";
    }
  } catch (error) {
    console.error('Error:', error);
  }
}
$('input').focus(function(){
  $(this).parents('.form-group').addClass('focused');
});

$('input').blur(function(){
  var inputValue = $(this).val();
  if ( inputValue == "" ) {
    $(this).removeClass('filled');
    $(this).parents('.form-group').removeClass('focused');  
  } else {
    $(this).addClass('filled');
  }
})  
async function successLogin() {
  document.getElementById('login-box').remove();
  document.getElementById('loading').style.animation="fade 0.5s linear forwards"
  document.getElementById('loading').style.display="inline";
  await delay(3000)
  window.location.href = './chat/'
}
function delay(milliseconds){
  return new Promise(resolve => {
      setTimeout(resolve, milliseconds);
  });
}

function getCookie(cookieName) {
  const cookies = document.cookie.split(';');
  for (const cookie of cookies) {
    const [name, value] = cookie.trim().split('=');
    if (name === cookieName) {
      return decodeURIComponent(value);
    }
  }
  return null;
}

if (getCookie('token') || getCookie('token') !== '') {
  makeAuthenticatedRequest()
}

async function makeAuthenticatedRequest() {
  const token = getCookie('token');

  try {
    fetch('/api/users.js', {
      method: 'GET',
      headers:   
      {'Content-Type': 'application/json',
      'username': getCookie('username'),}
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((userData) => {
        accountType = userData.type
        if (getCookie('username') !== userData.username) {
          Cookies.set('username', userData.username);
          window.location.reload();
        }
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
        if (window.location.hostname == '127.0.0.1') {
          //allow!!
        } else {

        }
      });

    const response = await fetch('/api/verify.js', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      const data = await response.json();
      console.log('Server response:', data);
      window.location.replace('/chat')
    } else {
      console.error('Server error:', response.status, response.statusText);
      if (window.location.hostname == '127.0.0.1') {
        //allow!!
      } else {

      }
    }
  } catch (error) {
    console.error('Error:', error);
    if (window.location.hostname == '127.0.0.1') {
      //allow!!
    } else {

    }
  }
}