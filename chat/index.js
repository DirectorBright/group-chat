let titlecnt = 1;
let contents1;
let username1;
let contents;
let username;
let addNew = true;
let lastAddedCont;
let lastAddedUser;
let notifications;
let lastMID;
document.cookie = 'chat=0'
const worker = new Worker('/chat/worker.js');
worker.postMessage('start');

if (Notification.permission !== "granted") {
  notifications = false
} else {
  notifications = true
  document.getElementById('notify').innerHTML = 'Notifications on'
}

document.getElementById('signout-confirm').style.display = 'None';

function userOp() {
  document.getElementById('userimg').src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + getCookie('username') + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b'
  document.getElementById('user-username').innerHTML = getCookie('username')
}
userOp()
function scrollDown() {
    var elem = document.getElementById('contain');
    elem.scrollTop = elem.scrollHeight;
}

const messageQueue = new Map();

function sendMessage(userId, message) {
  // Check if the user has sent a message within the last 2 seconds
  if (hasRecentlySentMessage(userId)) {
    console.log("Rate limit exceeded. Please wait before sending another message.");
    addNewMessage('Stop spamming', 'Please stop spamming!!! You are limited to 1 message every 2 seconds!!')
    document.getElementById('message').value = '';
    return;
  }

  // Add the message to the queue
  addToMessageQueue(userId, message);

  validate()
}

function hasRecentlySentMessage(userId) {
  // Check if the user has sent a message within the last 2 seconds
  const lastMessageTime = messageQueue.get(userId);
  if (!lastMessageTime) {
    return false;
  }

  const currentTime = Date.now();
  return currentTime - lastMessageTime < 2000;
}

function addToMessageQueue(userId, message) {
  // Add the current time to the queue
  messageQueue.set(userId, Date.now());

  console.log(`Storing message: "${message}" from user ${userId}`);
}

var wage = document.getElementById("message");
wage.addEventListener("keydown", function (e) {
    if (e.code === "Enter") {  //checks whether the pressed key is "Enter"
        sendMessage(getCookie('username'), 'Send?')
    }
});

function validate(e) {
    contents = document.getElementById('message').value;
    username = getCookie('username');
    if (contents == '') {
       return
    }
    if (contents.length > 225) {
      console.log('String is too big!')
      addNewMessage('Too big', 'This message is over 225 characters! Please shorten it or spread it out across more messages!')
      return
    }
    if (containsHTMLElements(contents)) {
      console.log('Contains DOM elements!')
      addNewMessage('Denied', 'This message contains DOM elements!')
      return
    }
    console.log('Requesting add new message with permametres: ' + username + ',' + contents)
    addNew = false;
    //add scripts about querying the database ._.
    if (username == 'TheJudge' && contents.includes('Edward escapes from John Canty')) {
      addNewMessage('Stop spamming', 'This message has been flagged as spam.')
      return
    }
    if (window.location.href.includes('127.0.0.1') == true) {
      addNew = true;
      addNewMessage(username, contents, btoa(Math.random() * 20))
    } else {
      updateServerlessFunction(username, contents);
    }
    //end
    document.getElementById('message').value = '';
}

function containsHTMLElements(inputString) {
  return false;
}

function addNewMessage(auth, cont, mID) {
  if (addNew === true) {
    if (auth == '' || cont == '') {
      console.log('Message was denied as it was unintentional')
      return;
    }
    if (auth == lastAddedUser) {
      const theirLastMessage = document.getElementById('cloneme');
      /*let height = theirLastMessage.style.height.replace('px', '')
      height = parseInt(height, 10)
      height = height+15
      theirLastMessage.style.height = `${height + 'px'}`*/
      theirLastMessage.querySelector('#contents').innerHTML = `${theirLastMessage.querySelector('#contents').innerHTML}<br>${cont}`
      scrollDown();
    } else {
      lastAddedCont = cont;
      lastAddedUser = auth;
      var elem = document.getElementById('cloneme')
      var clone = elem.cloneNode(true);
      elem.id = 'no-more'
      elem.class = 'messagething'
      clone.setAttribute("data", mID);
      lastMID = mID;

      var conts = clone.querySelector('#contents')
      var author = clone.querySelector('#author')
      var time = clone.querySelector('#timestamp')
      var pfp = clone.querySelector('#pfp')

      pfp.onclick = function() {
        goTo(auth);
      };
      const d = new Date();
      let ampm = d.getHours();
      let min = d.getMinutes();
      if (ampm > 12) {
          let ampm = " PM"
          console.log("PM")
          if (min < 10) {
            time.innerHTML = d.getHours()-12 + ":" + "0" + d.getMinutes() + ampm;
          } else {
            time.innerHTML = d.getHours()-12 + ":" + d.getMinutes() + ampm;
          }
          
      }
      if (ampm < 12) {
           ampm = " AM"
          console.log("AM")
          console.log(ampm)
          if (min < 10) {
            time.innerHTML = d.getHours() + ":" + "0" + d.getMinutes() + ampm;
          } else {
            time.innerHTML = d.getHours() + ":" + d.getMinutes() + ampm;
          }
      }

      var imgEncode = auth.replace(" ", "_")
      pfp.src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + imgEncode + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b';
      conts.innerHTML = cont;
      author.innerHTML = auth;

      elem.after(clone);
      scrollDown();

      document.title = ' (' + titlecnt + ') | Metnals Hsosptial'
      titlecnt++
      console.log('added new message with data: ' + auth + ',' + cont + ',titlecnt:' + titlecnt)
      console.log('showing notification....')
      // Check if the Page Visibility API is supported
      if (notifications == true) {
        if (typeof document.hidden !== 'undefined') {
          // Add an event listener to respond to visibility changes
          document.addEventListener('visibilitychange', function () {
            if (document.hidden) {
              // The page is not visible (user switched to another tab or minimized the browser)
              console.log('Tab is not visible, you get notified lmao');
              if ('Notification' in window) {
                // Request permission to display notifications
                Notification.requestPermission()
                  .then(function (permission) {
                    if (permission === 'granted') {
                      // Create and display a notification
                      const notification = new Notification(auth, {
                       body: cont,
                        icon: './notification.png', // Path to an icon for the notification
                      });
          
                      // Handle click on the notification
                      notification.onclick = function () {
                        // Handle the click event (e.g., open a specific URL)
                        window.open('https://metnals-hospital.vercel.app/chat/');
                  };
                }
              });
          }
            } else {
              // The page is visible (user switched back to the tab)
              console.log('Tab is visible');
            }
          });
        } 
      }
    }
  }
}
function goTo(n) {
  window.location.replace('/users/?user=' + n)
}

function isTabFocused() {
  // Check if the Page Visibility API is supported by the browser
  if (typeof document.hidden !== 'undefined') {
    return !document.hidden;
  } else if (typeof document.msHidden !== 'undefined') {
    return !document.msHidden;
  } else if (typeof document.webkitHidden !== 'undefined') {
    return !document.webkitHidden;
  } else {
    // If the Page Visibility API is not supported, assume the tab is focused
    return true;
  }
}


function updateServerlessFunction(username, message) {
    // Send the input data to the serverless function using a fetch request
    fetch('../api/message.js', {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + getCookie('token'),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username: username, message: message }), // Send 'username' and 'message'
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log('Serverless function response:', data);
        addNew = false;
      })
      .catch((error) => {
        console.error('Error:', error);
      });
        if (getCookie('chat') !== '0') {
          saveMessageDM(message);
          return
        }
        const data = { username, message };
fetch('../api/firestoreSave.js', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((data) => {
    console.log('Serverless function response:', data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
  }

  function saveMessageDM(mess) {
    const functionUrl = '/api/test.js';
const chatName = getCookie('chat');
const message = mess;
const requestBody = JSON.stringify({ message });

const headers = new Headers({
  'x-chat': chatName, // Include the chat name in the headers
  'Authorization': 'Bearer ' + getCookie('token'),
  'Content-Type': 'application/json',
});

fetch(functionUrl, {
  method: 'POST', // Change the method to POST
  headers: headers,
  body: requestBody,
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((chatData) => {
    console.log('Chat data:', chatData);
    // Handle the retrieved chat data as needed
  })
  .catch((error) => {
    console.error('Error posting a new message:', error);
  });
}

  function fetchData() {
    const Url = '../api/message.js';
    /*if (window.location.hostname == '127.0.0.1') {
      contents1 = 'e';
      username1 = '';
      return
    }*/

    // Create a function that fetches data and returns a promise
    function fetchDataFromApi(apiUrl, token) {
      const requestOptions = {
        method: 'GET', // You can specify the HTTP method (GET in this case)
        headers: {
          'Authorization': `Bearer ${token}`, // Include the "Authorization" header
          'Content-Type': 'application/json',
        },
      };
    
      return fetch(apiUrl, requestOptions)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json(); // Parse the JSON response
        });
    }
    
    // Fetch data from the API URL
    const token = getCookie('token');
    const dataPromise = fetchDataFromApi(Url, token);
    
    // Use Promise.all to wait for the promise to resolve
    return dataPromise
      .then((responseData) => {
    
        // Assign the data to the contents1 and username1 variables
        contents1 = responseData.message;
        username1 = responseData.username;
    
        return contents1;
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
        worker.postMessage('stop');
      });
    
  }

  function getCookie(cookieName) {
    const cookies = document.cookie.split(';');
  
    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
  
      // Check if the current cookie's name matches the desired cookieName
      if (name === cookieName) {
  
        return decodeURIComponent(value);
      }
    }
  
    return null;
  }


  function addMessageMaybe() {
    if(isTabFocused()) {
      titlecnt = 0;
      document.title = '(0) | Metnals Hsosptial'
    }
    fetchData();
    if (!username1 == '') {
      addNew = true;
      contents = contents1
      username = username1
      console.log('new message!')
      console.log('Adding new message with parametres: ' + username1 + contents1)
      addNewMessage(username1, contents1, btoa(Math.random() * 20))
    }
  }

document.getElementById('signout').addEventListener('click', () => {
  console.log('signout')
  document.getElementById('signout-confirm').style.display = 'Inline'
})
document.getElementById('signout-confirmed').addEventListener('click', () => {
      document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  window.location.reload()
})
document.getElementById('dont-signout').addEventListener('click', () => {
  document.getElementById('signout-confirm').style.display = 'none'
}) 

document.getElementById('notify').addEventListener('click', () => {
  console.log('notifications')
  if (notifications == true) {
    notifications = false
    document.getElementById('notify').innerHTML = 'Notifications off'

  } else {
    Notification.requestPermission();
    notifications = true
    document.getElementById('notify').innerHTML = 'Notifications on'
  }
})

worker.onmessage = function(event) {
  if (event.data === 'fetchMessages') {
    console.log('Fetching messages...');
    addMessageMaybe();
  }
};


document.getElementById('img-url').addEventListener('keydown', (key) => {
  if (key.code === 'Enter') {
    insert(document.getElementById('img-url').value);
  }
})
document.getElementById('img-confirm').addEventListener('click', () => {
  insert(document.getElementById('img-url').value);
})
function insert(img) {
  document.getElementById('message').value = document.getElementById('message').value + '<br><img class="img-message" src="' + img + '">'
  document.getElementById('img').style.display = 'none'
}
document.getElementById('contain-sendoptions-img-circle').addEventListener('click', () => {
  document.getElementById('img').style.display = 'inline'
})