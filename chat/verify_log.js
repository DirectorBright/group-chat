let token;
let verified = false;
let logged = false;
getCookie('token');
function getCookie(cookieName) {
    const cookies = document.cookie.split(';');

    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
  
      if (name === cookieName) {

        return decodeURIComponent(value);
      }
    }

    return null;
  }


async function makeAuthenticatedRequest() {
    const token = getCookie('token');
  
    try {
      fetch('/api/users.js', {
        method: 'GET',
        headers:   
        {'Content-Type': 'application/json',
        'username': getCookie('username'),}
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((userData) => {
          accountType = userData.type
          if (getCookie('username') !== userData.username) {
            Cookies.set('username', userData.username);
            window.location.reload();
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
          if (window.location.hostname == '127.0.0.1') {
            //allow!!
          } else {
            window.location.href = '/'
          }
        });

      const response = await fetch('/api/verify.js', {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log('Server response:', data);
        if (data.username !== getCookie('username')) {
          document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          throw new Error('Username response - cookie mismatch!!');
        }
        verified = true
      } else {
        console.error('Server error:', response.status, response.statusText);
        if (window.location.hostname == '127.0.0.1') {
          //allow!!
        } else {
          window.location.href = '/'
        }
      }
    } catch (error) {
      console.error('Error:', error);
      if (window.location.hostname == '127.0.0.1') {
        //allow!!
      } else {
        window.location.href = '/'
      }
    }
  }
  let titlecnt = 0;
  let toGo = 0;
  function scrollDown() {
    var elem = document.getElementById('contain');
    elem.scrollTop = elem.scrollHeight;
  }
  //addNewMessage('jackson', 'cont', '1')
  function addNewMessage(auth, cont, timestamp) {
    if (auth == '' || cont == '') {
      console.log('Message was denied as it was unintentional')
    } else {
      lastAddedCont = cont;
      var elem = document.getElementById('cloneme')
      var clone = elem.cloneNode(true);
      elem.id = 'no-more'
      elem.class = 'messagething'

      var conts = clone.querySelector('#contents')
      var author = clone.querySelector('#author')
      var time = clone.querySelector('#timestamp')
      var pfp = clone.querySelector('#pfp')

      pfp.onclick = function() {
        goTo(auth);
      };


      time.innerHTML = formatTimestamp(timestamp);
      conts.innerHTML = cont;
      author.innerHTML = auth;
      var imgEncode = auth.replace(" ", "_")
      pfp.src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + imgEncode + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b';

      elem.after(clone);
      scrollDown();

      document.title = ' (' + titlecnt + ') | Metnals Hsosptial'
      titlecnt++
      console.log('added new message with data: ' + auth + ',' + cont + ',titlecnt:' + titlecnt)
    }
}

  function formatTimestamp(timestamp) {
    // Create a Date object from the timestamp (in milliseconds)
    const date = new Date(timestamp * 1000); // Multiply by 1000 to convert seconds to milliseconds

    // Define month names for formatting
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    // Get individual date components
    const month = monthNames[date.getMonth()]; // 0-based month
    const day = date.getDate();
    let hours = date.getHours();
    let adjustedMinutes = date.getMinutes();

    // Determine whether it's AM or PM
    const period = hours >= 12 ? 'PM' : 'AM';

    // Adjust hours to 12-hour format
    if (hours > 12) {
        hours -= 12;
    }

    // Format minutes with zero-padding
    const minutes = adjustedMinutes.toString().padStart(2, '0');

    // Create a string in the desired format
    const formattedDate = `${month} ${day}, ${hours}:${minutes}${period}`;

    return formattedDate;
}
function delay(milliseconds){
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds);
    });
  }
function fetchLog() {
    fetch('../api/firestoreGet.js', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + getCookie('token'),
          'Content-Type': 'application/json', // Adjust content type as needed
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          console.log('Fetched documents:', data.documents);
          // Reverse the order of documents
          const reversedDocuments = data.documents.reverse();
          toGo = reversedDocuments.length;
          for (var i = 0; i < reversedDocuments.length; i++) {
            if (containsHTMLElements(reversedDocuments[i].message)) {
              console.warn('Contains DOM elements!')
            } else {
            addNewMessage(reversedDocuments[i].username, reversedDocuments[i].message, reversedDocuments[i].timestamp._seconds);
            lastAddedUser2 = reversedDocuments[i].username;
            }
          }
          logged = true
          scrollDown();
        })
        .catch((error) => {
          console.error('Error:', error);
        });
}

function containsHTMLElements(inputString) {
  return false;
}
  

async function start() {
  if (window.location.hostname == '127.0.0.1') {
    document.getElementById('loading').remove();
    return
  }
    await makeAuthenticatedRequest();
    await delay(500)
    await move();
    await fetchLog();
    await delay(500);
    let times;
    while (logged == false) {
      await delay(1)
      times++
      if (times > 30000) {
        verified = false
        logged = true
      }
    }
    document.getElementById('redir').innerHTML = 'Finalising...'
    if (logged == true && verified == true) {
        document.getElementById('redir').innerHTML = 'Success! Welcome, ' + getCookie('username') + '!'
        document.getElementById('loading').remove();
    } else {
      document.getElementById('redir').style.color = 'rgb(250, 0, 0)'
      document.getElementById('redir').innerHTML = 'Failed, proceed anyway?'
      document.getElementById('buttons-pa').style.display = 'inline'
      document.getElementById('pa-yes').addEventListener('click', () => {
        document.getElementById('loading').remove()
      })
      document.getElementById('pa-no').addEventListener('click', () => {
        window.location.href = '/'
      })
    }
}
async function move() {
  document.getElementById('redir').style.animation = 'up 0.5s ease-out alternate'
  await delay(400)
  document.getElementById('redir').style.opacity = 0
  
  document.getElementById('redir2').style.animation = 'in 0.5s ease-out alternate'
  await delay(500)
  document.getElementById('redir2').style.opacity = 1
  document.getElementById('redir2').style.translate = '500px'
  document.getElementById('redir2').style.filter = 'blur(0)'
}
document.addEventListener('DOMContentLoaded', start())
function goTo(n) {
  window.location.replace('/users/?user=' + n)
}