let int
self.onmessage = function(event) {
    if (event.data === 'start') {
      // Start an interval to signal the main thread to fetch messages
      int = setInterval(function() {
        self.postMessage('fetchMessages');
      }, 300);
    } else {
      if (event.data === 'stop') {
        clearInterval(int)
        console.warn('Worker ceased.')
      }
    }
  };
  