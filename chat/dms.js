document.getElementById('dms-button').addEventListener('click', () => {
    document.getElementById('dms-change').style.display = 'inline'
})
document.getElementById('dm-confirm').addEventListener('click', () => {
    beginSwitch();
})
let reciever
async function beginSwitch() {
    reciever = document.getElementById('toDM').value
    await createChat();
    await fetchChatDocument();
    await clearLog();
    await log();
    document.getElementById('dms-change').style.display = 'none'
}
function saveMessage(mess) {
    const functionUrl = '/api/test.js';
const chatName = getCookie('chat');
const message = mess;
const requestBody = JSON.stringify({ message });

const headers = new Headers({
  'x-chat': chatName, // Include the chat name in the headers
  'Authorization': 'Bearer ' + getCookie('token'),
  'Content-Type': 'application/json',
});

  document.getElementById('send').addEventListener('click', () => {
    fetch(functionUrl, {
  method: 'POST', // Change the method to POST
  headers: headers,
  body: requestBody,
})
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then((chatData) => {
    console.log('Chat data:', chatData);
    // Handle the retrieved chat data as needed
  })
  .catch((error) => {
    console.error('Error posting a new message:', error);
  });
  })
}

function getCookie(cookieName) {
    const cookies = document.cookie.split(';');
  
    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
  
      // Check if the current cookie's name matches the desired cookieName
      if (name === cookieName) {
  
        return decodeURIComponent(value);
      }
    }
    return null;
  }

function createChat() {
    const headers = {
        'Content-Type': 'application/json',
      };
      
      // Create a data object with the user and name fields
      const data = {
        user: reciever,  // Replace 'username' with the desired username
        name: Math.random() * 567,
      };
      
      fetch('/api/editChat.js', {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(data),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((chatData) => {
          console.log('Chat data:', chatData);
          reciever = chatData.name
        })
        .catch((error) => {
          console.error('Error posting a new message:', error);
        });
}

function log() {
  const functionUrl = '/api/test.js'
  const headers = new Headers({
    'x-chat': getCookie('chat'), // Include the chat name in the headers
    'Authorization': 'Bearer ' + getCookie('token'),
    'Content-Type': 'application/json',
  });
    fetch(functionUrl, {
        method: 'GET',
        headers: headers,
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((chatData) => {
          console.log('Chat data:', chatData);
          const reversedDocuments = chatData.messages.reverse();
          toGo = reversedDocuments.length;
          for (var i = 0; i < reversedDocuments.length; i++) {
            addNewMessage(reversedDocuments[i].username, reversedDocuments[i].message, reversedDocuments[i].timestamp._seconds);
          }
        })
        .catch((error) => {
          console.error('Error fetching chat data:', error);
        });
}
async function fetchChatDocument() {
  const userFromHeader = reciever; // Replace with your desired user

  const headers = {
    'Content-Type': 'application/json',
    Chat: userFromHeader,
  };

  try {
    const response = await fetch('/api/editChat.js', {
      method: 'GET',
      headers: headers,
    });

    if (response.ok) {
      const chatData = await response.json();
      console.log('Chat Data:', chatData);
      document.cookie = 'chat=' + chatData.name
    } else {
      console.error('Failed to fetch chat document:', response.statusText);
    }
  } catch (error) {
    console.error('Error fetching chat document:', error);
  }
}
function clearLog() {
    const elements = document.querySelectorAll('#no-more');
    elements.forEach(element => {
        element.parentNode.removeChild(element);
    });
}
function formatTimestamp(timestamp) {
    // Create a Date object from the timestamp (in milliseconds)
    const date = new Date(timestamp * 1000); // Multiply by 1000 to convert seconds to milliseconds

    // Define month names for formatting
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    // Get individual date components
    const month = monthNames[date.getMonth()]; // 0-based month
    const day = date.getDate();
    let hours = date.getHours();
    let adjustedMinutes = date.getMinutes();

    // Determine whether it's AM or PM
    const period = hours >= 12 ? 'PM' : 'AM';

    // Adjust hours to 12-hour format
    if (hours > 12) {
        hours -= 12;
    }

    // Format minutes with zero-padding
    const minutes = adjustedMinutes.toString().padStart(2, '0');

    // Create a string in the desired format
    const formattedDate = `${month} ${day}, ${hours}:${minutes}${period}`;

    return formattedDate;
}
let lastAddedUser;
function addNewMessage(auth, cont, timestamp) {
  if (auth == '' || cont == '') {
    console.log('Message was denied as it was unintentional')
  }
  if (auth == lastAddedUser) {
    const theirLastMessage = document.getElementById('cloneme');
    /*let height = theirLastMessage.style.height.replace('px', '')
    height = parseInt(height, 10)
    height = height+15
    theirLastMessage.style.height = `${height + 'px'}`*/
    theirLastMessage.querySelector('#contents').innerHTML = `${theirLastMessage.querySelector('#contents').innerHTML}<br>${cont}`
    scrollDown();
  } else {
    lastAddedCont = cont;
    var elem = document.getElementById('cloneme')
    var clone = elem.cloneNode(true);
    elem.id = 'no-more'
    elem.class = 'messagething'

    var conts = clone.querySelector('#contents')
    var author = clone.querySelector('#author')
    var time = clone.querySelector('#timestamp')
    var pfp = clone.querySelector('#pfp')

    pfp.onclick = function() {
      goTo(auth);
    };


    time.innerHTML = formatTimestamp(timestamp);
    conts.innerHTML = cont;
    author.innerHTML = auth;
    var imgEncode = auth.replace(" ", "_")
    pfp.src = 'https://firebasestorage.googleapis.com/v0/b/metnals.appspot.com/o/pfps%2F' + imgEncode + '.png?alt=media&token=e7019ce2-dae9-4183-a0e9-5fef7635366b';

    elem.after(clone);
    scrollDown();

    document.title = ' (' + titlecnt + ') | Metnals Hsosptial'
    titlecnt++
    console.log('added new message with data: ' + auth + ',' + cont + ',titlecnt:' + titlecnt)
  }
}
function goTo(n) {
  window.location.replace('/users/?user=' + n)
}